package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.services.CaregiverService;
import ro.tuc.ds2020.services.PatientService;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {
    public CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @PostMapping
    public ResponseEntity<CaregiverDTO> insertCaregiver (@Valid @RequestBody CaregiverDTO caregiverDTO) {
        CaregiverDTO caregiverDTO1 = CaregiverBuilder.toCaregiverDTO(caregiverService.insert(CaregiverBuilder.toCaregiver(caregiverDTO)));
        return new ResponseEntity<>(caregiverDTO1, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<CaregiverDTO>> getCaregivers () {
        return new ResponseEntity<>(caregiverService.showCaregivers(), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CaregiverDTO> getCaregiverById(@PathVariable("id") UUID id) {
        return new ResponseEntity<>(caregiverService.getCaregiverById(id), HttpStatus.OK);
    }

    @GetMapping(value = "/find-user/{username}")
    public ResponseEntity<CaregiverDTO> getCaregiverByUsername(@PathVariable("username") String username) {
        return new ResponseEntity<>(caregiverService.getCaregiverByUsername(username), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<UUID> deleteCaregiver (@PathVariable("id") UUID id)  {
        return new ResponseEntity<>(caregiverService.deleteCaregiverById(id), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<CaregiverDTO> updateCaregiver (@Valid @RequestBody CaregiverDTO caregiverDTO)  {
        return new ResponseEntity<>(CaregiverBuilder.toCaregiverDTO(caregiverService.update(CaregiverBuilder.toCaregiver(caregiverDTO))), HttpStatus.OK);
    }

}
