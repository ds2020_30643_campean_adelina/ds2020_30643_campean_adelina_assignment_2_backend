package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.services.MedicationPlanService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication-plan")
public class MedicationPlanController {

    public MedicationPlanService medicationPlanService;

    @Autowired
    public MedicationPlanController(MedicationPlanService medicationPlanService) {
        this.medicationPlanService = medicationPlanService;
    }

    @PostMapping
    public ResponseEntity<MedicationPlanDTO> insertMedicationPlan (@Valid @RequestBody MedicationPlanDTO medicationPlanDTO) {
        MedicationPlanDTO medicationPlanDTO1 = MedicationPlanBuilder.toMedicationPlanDTO(medicationPlanService.insert(MedicationPlanBuilder.toMedicationPlan(medicationPlanDTO)));
        return new ResponseEntity<>(medicationPlanDTO1, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<MedicationPlanDTO>> showMedicationPlans(){
        return new ResponseEntity<>(medicationPlanService.showMedicationPlan(), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<List<MedicationDTO>> getMedsMedicalPlan(@PathVariable("id") UUID id) {
        return new ResponseEntity<>(medicationPlanService.getMedsMedicalPlan(id), HttpStatus.OK);
    }

}
