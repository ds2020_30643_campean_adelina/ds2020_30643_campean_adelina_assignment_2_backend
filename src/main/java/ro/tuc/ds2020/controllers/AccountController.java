package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.AccountBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.services.AccountService;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/account")
public class AccountController {

    public AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping
    public ResponseEntity<AccountDTO> insertAccount (@Valid @RequestBody AccountDTO accountDTO) {
        return new ResponseEntity<>(AccountBuilder.toAccountDTO(accountService.insertAccount(AccountBuilder.toPatient(accountDTO))), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<AccountDTO>> getAccounts () {
        return new ResponseEntity<>(accountService.getAccounts(), HttpStatus.OK);

    }

    @GetMapping(value = "/{username}")
    public ResponseEntity<AccountDTO> getAccountByUsername(@PathVariable("username") String username) {
        return new ResponseEntity<>(AccountBuilder.toAccountDTO(accountService.getByUsername(username)), HttpStatus.OK);
    }

    //@PutMapping
    /*public ResponseEntity<AccountDTO> updateAccount  (@Valid @RequestBody AccountDTO accountDTO) {
        return new ResponseEntity<>(AccountBuilder.toAccountDTO(accountService.updateAccount(AccountBuilder.toPatient(accountDTO))), HttpStatus.OK);
    }
    */
   /* public ResponseEntity<String> updateAccount  (@Valid @RequestBody String username, String password) {
        return new ResponseEntity<String>((accountService.updateAcc(username, password)), HttpStatus.OK);
    }*/
}
