package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.services.PatientService;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    public PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {

        this.patientService = patientService;
    }

    @PostMapping
    public ResponseEntity<PatientDTO> insertPatient (@Valid @RequestBody PatientDTO patientDTO)  {
        PatientDTO patientDTO1 = PatientBuilder.toPatientDTO(patientService.insert(PatientBuilder.toPatient(patientDTO)));
        return new ResponseEntity<>(patientDTO1, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<Set<PatientDTO>> getPatients () {
        return new ResponseEntity<>(patientService.showPatients(), HttpStatus.OK);

    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PatientDTO> getPatientById(@PathVariable("id") UUID id) {
        return new ResponseEntity<>(patientService.getPatientById(id), HttpStatus.OK);
    }

    @GetMapping(value = "/find-user/{username}")
    public ResponseEntity<PatientDTO> getPatientByUsername(@PathVariable("username") String username) {
        return new ResponseEntity<>(patientService.getPatientByUsername(username), HttpStatus.OK);
    }

   /* @DeleteMapping
    public ResponseEntity<PatientDTO> deletePatient (@Valid @RequestBody PatientDTO patientDTO) {
        return new ResponseEntity<>(PatientBuilder.toPatientDTO(patientService.deletePatient(PatientBuilder.toPatient(patientDTO))), HttpStatus.OK);
    }
*/
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<UUID> deletePatientById (@PathVariable("id") UUID id) {
        return new ResponseEntity<>(patientService.deletePatientById(id), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<PatientDTO> updatePatient (@Valid @RequestBody PatientDTO patientDTO)  {
        System.out.println("patient:- > " + patientDTO.getMedicationPlans());
        return new ResponseEntity<>(PatientBuilder.toPatientDTO(patientService.updatePatient(PatientBuilder.toPatient(patientDTO))), HttpStatus.OK);
    }


}
