package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import ro.tuc.ds2020.dtos.MessageActivityDTO;

import java.util.UUID;

@Controller
public class SchedulerController {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;


    public void sendMessage(MessageActivityDTO activity){
        messagingTemplate.convertAndSend("/caregiver/notification", activity); //new MessageActivityDTO(UUID.fromString("36dd2703-68c1-4bf0-87b6-b0abda844179"), UUID.fromString("f060d306-cc95-4a2a-ab01-d352a989021c"), "2011-11-28 02:27:59","2011-11-28 10:18:11","Sleeping"));

    }

}
