package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.ManyToAny;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name="MedicationPlan")
public class MedicationPlan {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {
            CascadeType.MERGE
    })
    @JoinTable(name = "meds",
            joinColumns = @JoinColumn(name = "MedicationPlanId"),
            inverseJoinColumns = @JoinColumn(name = "MedicationId")
    )
    private List<Medication> medicationList;

    @Column(name = "intakeIntervals", nullable = false)
    private String intakeIntervals;

    @Column(name = "treatmentPeriod", nullable = false)
    private String treatmentPeriod;

    public MedicationPlan(UUID id, List<Medication> medicationList, String intervals, String treatmentPeriod) {
        this.id = id;
        this.medicationList = medicationList;
        this.intakeIntervals = intervals;
        this.treatmentPeriod = treatmentPeriod;
    }

    public MedicationPlan() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public List<Medication> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(List<Medication> medicationList) {
        this.medicationList = medicationList;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public String getTreatmentPeriod() {
        return treatmentPeriod;
    }

    public void setTreatmentPeriod(String treatmentPeriod) {
        this.treatmentPeriod = treatmentPeriod;
    }
}
