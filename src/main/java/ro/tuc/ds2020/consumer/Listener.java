package ro.tuc.ds2020.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.SchedulerController;
import ro.tuc.ds2020.dtos.MessageActivityDTO;
import ro.tuc.ds2020.dtos.builders.MessageActivityBuilder;
import ro.tuc.ds2020.services.MessageActivityService;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Service
public class Listener {

    private static final Logger log = LoggerFactory.getLogger(Listener.class);
    private final SchedulerController schedulerController;
    private final MessageActivityService messageActivityService;

    @Autowired
    public Listener(SchedulerController schedulerController, MessageActivityService messageActivityService) {
        this.schedulerController = schedulerController;
        this.messageActivityService = messageActivityService;
    }

    @RabbitListener(queues="Queue")
    public void consumeMessage(final Message message) throws ParseException {
       // System.out.println("Message received! "+ message.toString());

        String mesaj = new String(message.getBody());

        String message_id = mesaj.substring(19,55);
        //System.out.println("id: "+ message_id );

        String patient_id = mesaj.substring(68,104);
        //System.out.println("patient "+ patient_id);

        String start = mesaj.substring(118,137);
        //System.out.println("start " + start);

        String end = mesaj.substring(150, 169);
       // System.out.println("end "+ end);

        String activity = mesaj.substring(188,mesaj.length()-2);
        //System.out.println("activity "+activity);

        MessageActivityDTO messageActivityDTO = new MessageActivityDTO(UUID.fromString(message_id),UUID.fromString(patient_id), start, end, activity);

        //System.out.println("activity:" + messageActivityDTO.getActivity_label());

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date start_time = format.parse(start);
        //System.out.println(start_time);

        Date end_time = format.parse(end);
       // System.out.println(end_time);

        if((activity.equals("Sleeping"))&&( end_time.getTime() - start_time.getTime() > 25200000)) {
            System.out.println("Pacientul are o problema, doarme prea mult!!");
          //  messageActivityService.insert(MessageActivityBuilder.toMessageActivity(messageActivityDTO));
            schedulerController.sendMessage(messageActivityDTO);
        }


        if((activity.equals("Leaving"))&&( end_time.getTime() - start_time.getTime() > 18000000)) {
            System.out.println("Pacientul are o problema, sta prea mult afara!!");
         //   messageActivityService.insert(MessageActivityBuilder.toMessageActivity(messageActivityDTO));
            schedulerController.sendMessage(messageActivityDTO);
        }


         if((activity.equals("Showering")||(activity.equals("Toileting"))||(activity.equals("Grooming")))&&( end_time.getTime() - start_time.getTime() > 1800000)) {
            System.out.println("Pacientul are o problema, sta prea mult la baie!!");
            // messageActivityService.insert(MessageActivityBuilder.toMessageActivity(messageActivityDTO));
            schedulerController.sendMessage(messageActivityDTO);
        }
    }


}
