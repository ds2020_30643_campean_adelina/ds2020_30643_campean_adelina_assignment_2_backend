package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MessageActivityDTO;
import ro.tuc.ds2020.entities.MessageActivity;

public class MessageActivityBuilder {

    public MessageActivityBuilder() {
    }

    public static MessageActivityDTO toMessageActivityDTO (MessageActivity messageActivityDTO) {
        return new MessageActivityDTO(messageActivityDTO.getMessage_id(), messageActivityDTO.getPatient_id(),messageActivityDTO.getStart_time(), messageActivityDTO.getEnd_time(),messageActivityDTO.getActivity_label());
    }

    public static MessageActivity toMessageActivity (MessageActivityDTO messageActivity) {
        return new MessageActivity(messageActivity.getMessage_id(),messageActivity.getPatient_id(),messageActivity.getStart_time(),messageActivity.getEnd_time(),messageActivity.getActivity_label());
    }

}
