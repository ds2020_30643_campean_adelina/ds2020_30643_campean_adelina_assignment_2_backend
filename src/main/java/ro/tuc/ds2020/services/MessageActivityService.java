package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.dtos.MessageActivityDTO;
import ro.tuc.ds2020.dtos.builders.AccountBuilder;
import ro.tuc.ds2020.dtos.builders.MessageActivityBuilder;
import ro.tuc.ds2020.entities.MessageActivity;
import ro.tuc.ds2020.repositories.MessageActivityRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MessageActivityService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final MessageActivityRepository messageActivityRepository;


    @Autowired
    public MessageActivityService(MessageActivityRepository messageActivityRepository) {
        this.messageActivityRepository = messageActivityRepository;
    }

    public MessageActivity insert (MessageActivity messageActivity) {
        return messageActivityRepository.save(messageActivity);
    }


    public List<MessageActivityDTO> getActivities() {
        return messageActivityRepository.findAll().stream().map(messageActivity -> MessageActivityBuilder.toMessageActivityDTO(messageActivity)).collect(Collectors.toList());
    }

}
