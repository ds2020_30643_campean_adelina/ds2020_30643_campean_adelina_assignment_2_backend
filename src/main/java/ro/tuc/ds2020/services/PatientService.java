package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PatientService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final PatientRepository patientRepository;
    private final MedicationPlanRepository medicationPlanRepository;

    private AccountService accountService;

    @Autowired
    public PatientService(PatientRepository patientRepository, MedicationPlanRepository medicationPlanRepository, AccountService accountService) {
        this.patientRepository = patientRepository;
        this.medicationPlanRepository = medicationPlanRepository;
        this.accountService = accountService;
    }

    public Patient insert(Patient patient) {

        return patientRepository.save(patient);
    }

    public Set<PatientDTO> showPatients() {

        List<Patient> patients = patientRepository.findAll();
        return patients.stream().map(patient -> PatientBuilder.toPatientDTO(patient)).collect(Collectors.toSet());
    }

    public Patient deletePatient(Patient patient) {
        patientRepository.deleteById(patient.getId());
        return patient;
    }

    public UUID deletePatientById(UUID id) {
        patientRepository.deleteById(id);
        return id;
    }

    public PatientDTO getPatientById(UUID id) {
        Set<PatientDTO> patients = showPatients();
        PatientDTO pdto = new PatientDTO();
        for(PatientDTO patientDTO:patients) {
            if(patientDTO.getId().equals(id)) {
                pdto = patientDTO;
                break;
            }
        }
        return pdto;
    }

    public PatientDTO getPatientByUsername(String username) {
        Set<PatientDTO> patients = showPatients();
        PatientDTO pdto = new PatientDTO();
        for(PatientDTO patientDTO:patients) {
            if(patientDTO.getUsername().equals(username)) {
                pdto = patientDTO;
                break;
            }
        }
        return pdto;
    }

    public Patient updatePatient(Patient patient) {
        List<Patient> patients = patientRepository.findAll();
        Patient p = new Patient();
        System.out.println("id-ul p "+patient.getId());
        for(Patient patient1:patients) {

            if (patient1.getId().equals(patient.getId())) {
                p = patient1;
                break;
            }
        }

        if (patient.getAddress()!= null) {
            p.setAddress(patient.getAddress());
        }

            if(patient.getBirthdate().toString()!= null) {
                p.setBirthdate(patient.getBirthdate());
            }else
                p.setBirthdate(p.getBirthdate());

            if(patient.getGender()!= null) {
                p.setGender(patient.getGender());
            }else
                p.setGender(p.getGender());

            if(patient.getName()!= null) {
                p.setName(patient.getName());
            }else
                p.setName(p.getName());

            if(patient.getMedicalRecord()!= null)
                p.setMedicalRecord(patient.getMedicalRecord());

            String pass = patient.getPassword();
            String user = patient.getUsername();


            if( (!(pass.isEmpty())) && (!(user.isEmpty())) )
                accountService.updateAcc(p.getUsername(), user, pass);

            else if(!pass.isEmpty() && user.isEmpty())
                accountService.updateAcc(p.getUsername(),p.getUsername(),pass);

            else if(pass.isEmpty() && !user.isEmpty())
                accountService.updateAcc(p.getUsername(),user,p.getPassword());

            if(!patient.getUsername().isEmpty()) {
                p.setUsername(patient.getUsername());}

            if(!patient.getPassword().isEmpty()) {
                p.setPassword(patient.getPassword());}

            if(patient.getMedicationPlans() != null) {


                    List<MedicationPlan> listaMed = p.getMedicationPlans();

                for(MedicationPlan medicationPlan :  patient.getMedicationPlans()) {

                    MedicationPlan medicationPlan0 = medicationPlanRepository.save(medicationPlan);


                    listaMed.add(medicationPlan0);

                    p.setMedicationPlans(listaMed);
                }

            }

            patientRepository.save(p);

        return p;
    }

}
