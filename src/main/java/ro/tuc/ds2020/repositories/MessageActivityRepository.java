package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.entities.MessageActivity;

import java.util.UUID;

public interface MessageActivityRepository extends JpaRepository<MessageActivity, UUID> {
}
